import time
import re
import os
import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('fileName')

    args = parser.parse_args()

    regex = re.compile(r'\((\d+\.\d+)\)\s+(\S+)\s+(\S+)\s+\[\d\]\s+(.*)$')
    with open(args.fileName, 'r') as file:
        while True:
            line = file.readline()
            match = regex.search(line)
            if match:
                print(match.groups())
            else:
                print(f'This line was unparsable: {line}')
            (delay_s,port_name,id,data) = match.groups()
            data = data.replace(' ', '')
            os.system(f'cansend {port_name} {id}#{data}')
            time.sleep(float(delay_s))

if __name__ == "__main__":
    main()
