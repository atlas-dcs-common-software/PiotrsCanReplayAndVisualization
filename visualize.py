# @author pnikiel

import time
import re
import os
import drawSvg as draw

class Visualization:
    def __init__(self):
        self.buses = {}
        self.timescale = 3000 # pixels per second
        self.d = draw.Drawing(10000, 500, displayInline=False)
        quanta = 0.01 # in seconds
        for i in range(5000):
            t = quanta * float(i)
            x = self.time_to_x(t)
            self.d.append(draw.Line(x, 0, x, 400, stroke='#77777766'))

        self.d.append(draw.Text(f'Horizontal grid is {1000*quanta} ms', 16, 10, 430 ))

        types = ['SYNC', 'TPDO1', 'TPDO3', 'NG.req', 'NG.rep']
        for (i, t) in enumerate(types):
            x = 50 + i*100
            self.d.append(draw.Rectangle(x, 450, 80, 40, fill=self.type_to_style(t)))
            self.d.append(draw.Text(t, 16, x + 10, 460, fill='white' ))


    def time_to_x(self, time):
        return 100 + self.timescale*time

    def bus_to_y(self, bus):
        if bus not in self.buses:
            self.buses[bus] = {'no': len(self.buses)}
        y = self.buses[bus]['no'] * 50
        return y

    def id_to_type(self, id, data):
        i = int(id, base=16)
        if i == 0x80:
            return 'SYNC'
        if i > 0x180 and i < 0x200:
            return 'TPDO1'
        if i > 0x380 and i < 0x400:
            return 'TPDO3'
        elif i > 0x700 and i < 0x73F:
            if data == "remoterequest":
                return "NG.req"
            else:
                return "NG.rep"
        else:
            return '?'

    def type_to_style(self, type):
        type_to_color = {
            'SYNC' : 'blue',
            'TPDO1' : '#ff008888',
            'TPDO3' : 'orange',
            'NG.req' : '#00999999',
            'NG.rep' : '#00BB0099',
            '?' : 'black'
        }
        return type_to_color[type]

    def parseMessage(self, time, port_name, id, data):
        pos_x = self.time_to_x(time)
        pos_y = self.bus_to_y(port_name)

        msg_dur = 0.00064 # 1 ms, to be calculated from the message type
        type = self.id_to_type(id, data)
        color = self.type_to_style(type)
        node = int(id, base=16) & 127
        h = 30
        if type == 'SYNC':
            h=40
        r = draw.Rectangle(pos_x,pos_y,msg_dur*self.timescale,h, stroke='black', fill=color, stroke_width=0.2)
        r.appendTitle(f'#{node} {type}')  # Add a tooltip
        self.d.append(r)
        pass

    def writeSvg(self):
        print(repr(self.buses))
        for (bus, info ) in self.buses.items():
            print(bus, info)
            y = self.bus_to_y(bus)
            # CAN bus label
            self.d.append(draw.Text(bus, 16, 0, y+2))
            # the axis
            self.d.append(draw.Line(0, y, 2000, y, stroke='black'))
        self.d.saveSvg('example.svg')


def main():
    regex = re.compile(r'\((\d+\.\d+)\)\s+(\S+)\s+(\S+)\s+\[\d\]\s+(.*)$')
    with open('cicusal1_canopen_261121_10_57.txt', 'r') as file:
        current_time = 0
        num_lines = 0
        v = Visualization()
        while num_lines < 20000:
            line = file.readline()
            match = regex.search(line)
            if match:
                print(match.groups())
            else:
                print(f'This line was unparsable: {line}')
            (delay_s,port_name,id,data) = match.groups()
            data = data.replace(' ', '')
            v.parseMessage(current_time, port_name, id, data)
            num_lines += 1
            current_time += float(delay_s)
    #v.setPixelScale(1)
    v.writeSvg()

if __name__ == "__main__":
    main()
